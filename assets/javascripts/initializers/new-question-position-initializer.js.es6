export default {
  name: "new-question-position-initializer",

  initialize: function(container) {
    var tbfNewTopic = function () {
      var composerController = Discourse.__container__.lookup('controller:composer');
      var username = window.location.pathname.split('/')[2];

      composerController.open({
          action:      Discourse.Composer.CREATE_TOPIC,
          usernames:   username,
          archetypeId: 'regular',
          draftKey:    Discourse.Composer.CREATE_TOPIC
      }).then(function () {
          return '';
      }).then(function (q) {
          composerController.appendText('');
      });
    };

    $('#ivan-create-topic').find('i').remove();
    
    $(document).ready(function(){

      $('header').on('click', '#ivan-create-topic', function(){
        if(Discourse.User.current()){
          tbfNewTopic();
        } else{
          var query = location.href;
          location.href = 'https://twobyfore.com/join/?try=comment' + '&redirectTo=' + query;
        }
      });

      $('body').on('click', '.custom-like, .like, .create.fade-out', function() {
        if (!Discourse.User.current()){
          ga('send', 'event', 'navigation', 'like-loggedOut', 'discourse');
          var query = window.location.href;
          location.href = "https://twobyfore.com/join/?try=comment" + "&redirectTo=" + query;
        }
        else if (Discourse.User.current()) {
          ga('send', 'event', 'navigation' ,'liked-loggedIn', 'discourse');
        }
      });
    });  
  }
}
