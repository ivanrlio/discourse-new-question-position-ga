# name: New Question Position
# about: Move +New Question button to <header> and to the right of the user avatar
# version: 1.0
# authors: Ivan RL
# url: https://ivanrlio@bitbucket.org/ivanrlio/discourse-new-question-position.git

register_asset "stylesheets/common/base/header.scss"